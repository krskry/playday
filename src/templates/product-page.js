import React from "react"
import { Link, graphql } from "gatsby"
import SEO from "../components/seo";

import Layout from "../components/layout"
import SubpageHero from "../sections/SubpageHero";
import { useTranslation } from "react-i18next";
import ProductDescription from "../sections/ProductDescription"

const BlogPostTemplate = ({ data, pageContext, location }) => {
  const siteTitle = data.site.siteMetadata.title
  const { t } = useTranslation();

  const { previous, next } = pageContext
  let post = data

  post.url = `/portfolio${post.markdownRemark.fields.slug}`;
  post.type = post.markdownRemark.frontmatter.date;
  post.durationText = post.markdownRemark.frontmatter.description;
  post.title = post.markdownRemark.frontmatter.title;
  post.imageSrc = post.markdownRemark.frontmatter.featuredImage.childImageSharp.fluid.src;
  post.locationText = post.markdownRemark.frontmatter.place;
  post.html = post.markdownRemark.html

  return (
    <Layout location={location} title={siteTitle}>
      <SEO
        title={post.markdownRemark.frontmatter.title}
        description={post.markdownRemark.frontmatter.description}
      />
      <div style={{ minHeight: "100vh", paddingTop: 100 }}>
        <SubpageHero title={post.title} subtitle={post.durationText} />
        <ProductDescription post={post}/>
      </div>
      <nav>
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <li>
            {previous && (
              <Link to={`/product${previous.fields.slug}`} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li>
            {next && (
              <Link to={`/product${next.fields.slug}`} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </li>
        </ul>
      </nav>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query productPageBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        featuredPost
        galleryImages {
            childImageSharp {
                fluid(quality: 99) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
        featuredImage {
            childImageSharp {
                fluid(quality: 99) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
      }
      fields {
          slug
      }
    }
  }
`