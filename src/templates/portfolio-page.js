import React from "react";
import { Link, graphql } from "gatsby";
import SEO from "../components/seo";
import parse from "html-react-parser";
import SubpageHero from "../sections/SubpageHero";

import Layout from "../components/layout";

const BlogPostTemplate = ({ data, pageContext, location }) => {
  const siteTitle = data.site.siteMetadata.title;
  const { previous, next } = pageContext;

  let post = data;

  post.url = `/portfolio${post.markdownRemark.fields.slug}`;
  post.type = post.markdownRemark.frontmatter.date;
  post.durationText = post.markdownRemark.frontmatter.description;
  post.title = post.markdownRemark.frontmatter.title;
  post.imageSrc =
    post.markdownRemark.frontmatter.featuredImage.childImageSharp.fluid.src;
  post.locationText = post.markdownRemark.frontmatter.place;
  post.html = post.markdownRemark.html;

  return (
    <Layout location={location} title={siteTitle}>
      <SEO
        title={post.markdownRemark.frontmatter.title}
        description={post.markdownRemark.frontmatter.description}
      />
      <div style={{ minHeight: "100vh" }}>
        <SubpageHero title={post.title} subtitle={post.durationText} />

        <img src={post.imageSrc} />
        {parse(post.html)}
      </div>
      <nav>
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0
          }}
        >
          <li>
            {previous && (
              <Link to={`/portfolio${previous.fields.slug}`} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li>
            {next && (
              <Link to={`/portfolio${next.fields.slug}`} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </li>
        </ul>
      </nav>
    </Layout>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        featuredPost
        place
        featuredImage {
          childImageSharp {
            fluid(quality: 99) {
              ...GatsbyImageSharpFluid_withWebp_noBase64
            }
          }
        }
      }
      fields {
        slug
      }
    }
  }
`;
