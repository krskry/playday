import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useTranslation } from "react-i18next";

// import device from "../../utils/device";

const Item = styled.div`
font-weight:700;
cursor:pointer;
margin:0 5px
;`


function LanguageSwitch({ className }) {
    const { t, i18n } = useTranslation();
    const language = i18n.language;
    const [currItem, setItem] = useState("pl");

    function handleChange(lang) {
        i18n.changeLanguage(lang);
    }

    useEffect(() => {
        if (language.includes("en")) {
            setItem("en");
        } else {
            setItem("pl");
        }
    }, [language]);

    return (
        <div className={className}>
            <Item
                onClick={() => handleChange("pl")}
                isActive={currItem === "pl"}
            >
                {t("shared.polish")}
            </Item>
            <Item
                onClick={() => handleChange("en")}
                isActive={currItem === "en"}
            >
                {t("shared.english")}
            </Item>
        </div>
    );
}

export default styled(LanguageSwitch)`
  display: flex;
  justify-content: center;
  align-items:center;
  margin-left:40px;
//   @media ${device.laptop} {
//     margin: 20px auto;
//   }
`;
