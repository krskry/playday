import React from "react";
import styled from "styled-components";
import Partner1 from "../assets/svg/Partner1.svg";
import Partner2 from "../assets/svg/Partner2.svg";

const Wrap = styled.section`
  height: 78px;
  width: 100%;
  background: #f7f7f7;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    margin: 0 24px;
  }
`;

const parners = () => {
  return (
    <Wrap>
      <Partner1 />
      <Partner2 />
      <Partner1 />
      <Partner2 />
    </Wrap>
  );
};

export default parners;
