import React from "react";
import styled from "styled-components";
import tw from "twin.macro";

import Quotes from "../assets/svg/Quotes.svg";
import QuotesShape from "../assets/svg/QuotesShape.svg";
import ClientPhoto from "../assets/clientTestimonials.png";

const Wrap = styled.div`
  ${tw`hidden md:flex`};
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  div {
    margin: 112px 0;
    display: flex;
  }
  h2 {
    ${tw`text-4xl sm:text-5xl font-black tracking-wide text-center`}
    span {
      color: #83ba26;
    }
  }
  p {
    margin-left: 24px;
    font-style: italic;
    font-size: 20px;
    color: #6c757d;
    max-width: 520px;
    display: flex;
    align-items: center;
  }
  img {
    z-index: 5;
  }
`;

const StyledQoutes = styled(Quotes)`
  transform: translate(250px, -50px);
`;

const StyledQuotesShape = styled(QuotesShape)`
  transform: translate(-130px, 120px);
`;

export default function testimonials(props) {
  return (
    <Wrap>
      <h2>
        Co o nas mowią <span>klienci</span>
      </h2>
      <div>
        <StyledQoutes />
        <img src={ClientPhoto} alt="" />
        <p>
          Najwyższa jakość, najlepsze komponenty, rygorystyczne normy
          bezpieczeństwa - to dla nas najwyższe priorytety.
        </p>
        <StyledQuotesShape />
      </div>
    </Wrap>
  );
}
