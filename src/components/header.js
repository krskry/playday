import React, { useState, useEffect } from "react";

import { useLocation } from "@reach/router";
import styled, { css } from "styled-components";
import { useTranslation } from "react-i18next";
import Link from "./link";
import Logo from "../assets/svg/logo.svg";
import LogoAlt from "../assets/svg/LogoAlt.svg";

import { useStickyNav, useScrollDirection } from "../hooks/index";
import LanguageSwitch from "./LanguageSwitch";
import tailwind from "../../tailwind.config";

const StyledNavbar = styled.nav`
  z-index: 20;
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  background: transparent;
  color: white;
  z-index: 20;
  font-size: 18px;
  padding: 0;
  transition: 0.1s ease;
  display: flex;
  background: ${(props) =>
    props.isStickyNav ? "rgba(41, 96, 147, .95) " : "transparent"};
  ${(props) =>
    props.isAlt &&
    css`
      background: white;
      color: ${tailwind.theme.colors.black};
    `}
  .navbar-nav .nav-link {
    color: white;
    letter-spacing: 1.28px;
    text-transform: uppercase;
  }
  > div {
    width: 100%;
    display: flex;
    margin: 0;
    max-width: 140rem;
    padding: 0.5rem 5rem;
    justify-content: space-between;
  }

  a {
    margin: 0 10px;
    text-transform: uppercase;
    border-bottom: 2px solid transparent;
    height: 25px;
    &[aria-current="page"],
    &:hover {
      border-bottom: ${(props) =>
        props.isAlt ? "1.5px black solid" : "1.5px solid white"};
    }
  }
  .menu {
    display: flex;
    align-items: center;
    @media (max-width: ${tailwind.theme.screens.xl}) {
      display: none;
    }
  }
`;

const Header = (props) => {
  const scrollDir = useScrollDirection();
  const location = useLocation();
  const [isAlt, setAlt] = useState(true);
  const [isOpen, setIsOpen] = useState(false);
  const isStickyNav = useStickyNav();
  const toggle = () => setIsOpen(!isOpen);
  const { t } = useTranslation();

  const navLinks = [
    {
      name: t("shared.nav_link0"),
      url: "/",
    },
    {
      name: t("shared.nav_link1"),
      url: "/about",
    },
    {
      name: t("shared.nav_link2"),
      url: "/products",
    },
    {
      name: t("shared.nav_link3"),
      url: "/portfolio",
    },
    {
      name: t("shared.nav_link7"),
      url: "/contact",
    },
  ];

  useEffect(() => {
    if (location.pathname === "/") {
      setAlt(false);
    } else if (isAlt) {
      setAlt(true);
    }
  }, [location, isAlt]);

  return (
    <StyledNavbar isStickyNav={isStickyNav} isAlt={isAlt}>
      <div className="logo-box">
        <div className="navbar-brand mr-auto">
          {isAlt ? <LogoAlt /> : <Logo />}
        </div>
        <div className="menu">
          {navLinks.map((item) => (
            <Link key={item.url} to={item.url}>
              {item.name}
            </Link>
          ))}
          <LanguageSwitch />
        </div>
      </div>
    </StyledNavbar>
  );
};

export default Header;
