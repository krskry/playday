import React from "react";
import { CSSTransition } from "react-transition-group";
import { gsap } from "gsap";

export default function transition({ children, start }) {
  const onEnter = (node) => {
    gsap.from(
      [node.children[0].firstElementChild, node.children[0].lastElementChild],
      0.6,
      {
        y: 30,
        delay: 0.6,
        ease: "power3.InOut",
        opacity: 0,
        stagger: {
          amount: 0.6,
        },
      }
    );
  };

  const onExit = (node) => {
    gsap.to(
      [node.children[0].firstElementChild, node.children[0].lastElementChild],
      0.6,
      {
        y: -30,
        ease: "power3.InOut",
        stagger: {
          amount: 0.2,
        },
      }
    );
  };

  return (
    <React.Fragment>
      <CSSTransition
        in={start}
        timeout={1200}
        classNames="transition"
        onExit={onExit}
        onEntering={onEnter}
        unmountOnExit
      >
        <div>{children}</div>
      </CSSTransition>
    </React.Fragment>
  );
}
