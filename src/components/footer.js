import React from "react";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import tw from "twin.macro";
import { SecondaryButton } from "../treact/components/misc/Buttons";

import Link from "./link";
import LogoInverse from "../assets/svg/logoInverse.svg";
import Planets from "../assets/Planets.png";
import SocialLinks from "./SocialLinks";

const StyledButton = tw(SecondaryButton)`w-full mt-24 w-48`;

const Footer = styled.footer`
  display: flex;
  flex-direction: column;
  padding: 3rem 0;
  justify-content: space-between;
  background: #f8f9fa;
  min-height: 750px;
  background: rgba(41, 96, 147, 0.95);
  position: relative;
  overflow: hidden;
  > div {
    min-height: 100%;
    display: flex;
  }
  a,
  a:hover {
    color: white;
  }
  ul {
    -webkit-padding-start: 0;
    padding: 0;
    & > li {
      color: white;
      list-style: none;
      a,
      a:hover {
        color: white;
      }
    }
  }
  .planetImage {
    position: absolute;
    bottom: -100px;
    right: -100px;
  }
  .container {
    max-width: 140rem;
    padding: 2rem 5rem;
  }
  .row {
    display: flex;
  }
`;

const Container = tw.div`w-full max-w-7xl pt-8 pb-8 pl-20 pr-20`;
const Column = tw.div`px-4 sm:px-0 sm:w-1/4 md:w-auto mt-12 mr-auto`;
const Row = tw.div`w-full flex flex-wrap text-center sm:text-left justify-center sm:justify-start md:justify-between -mt-12`;
const DateRow = tw.div` w-full max-w-7xl pt-8 pl-20 pr-20 flex justify-center text-white mb-2`;
const EstLabel = tw.div`p-2 mt-6 text-white max-w-144`;
const MarketingSlogan = tw.div`text-white p-2 pt-5 max-w-md  `;
const Title = tw.div`text-white text-xl mb-5`;
const ContentLinks = tw.div`w-48 w-full max-w-7xl pl-20 pr-20 flex justify-center text-white`;
const StyledLink = styled(Link)`
  ${tw`leading-loose  hover:border-b-1 border-white`}
`;

const FooterLink = ({ to, children }) => (
  <li>
    <StyledLink to={to}>{children}</StyledLink>
  </li>
);

export default () => {
  const date = new Date().getFullYear();
  const { t } = useTranslation();
  return (
    <Footer>
      <img src={Planets} alt="" className="planetImage" />
      <Container>
        <Row>
          <Column>
            <LogoInverse />
            <EstLabel>{t("shared.footer_below_logo")}</EstLabel>
            <div className="mt-5">
              <MarketingSlogan>
                {t("shared.footer_marketing_slogan")}
              </MarketingSlogan>
              <div className="d-flex mt-5">
                <SocialLinks isAlt />
              </div>
            </div>
          </Column>
          <Column style={{ minHeight: 300 }}>
            <Title>{t("shared.footer_title1")}</Title>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_1_item1")}</FooterLink>
            </ul>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_1_item2")}</FooterLink>
            </ul>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_1_item3")}</FooterLink>
            </ul>
          </Column>
          <Column style={{ minHeight: 300 }}>
            <Title>{t("shared.footer_title2")}</Title>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_2_item1")}</FooterLink>
            </ul>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_2_item2")}</FooterLink>
            </ul>
          </Column>
          <Column style={{ minHeight: 300 }}>
            <Title>{t("shared.footer_title3")}</Title>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_3_item1")}</FooterLink>
            </ul>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_3_item2")}</FooterLink>
            </ul>
            <ul>
              <FooterLink to="/">{t("shared.footer_title_3_item3")}</FooterLink>
            </ul>
            <ul>
              <StyledButton to="/">{t("shared.footer_cta")}</StyledButton>
            </ul>
          </Column>
        </Row>
      </Container>
      <div className="d-flex w-full flex-col">
        <DateRow>{date}</DateRow>
        <ContentLinks>
          <Link className="mx-2" to="/">
            {t("shared.footer_privacy_policy")}
          </Link>
          <Link className="mx-2" to="/">
            {t("shared.footer_cookies")}
          </Link>
        </ContentLinks>
      </div>
    </Footer>
  );
};
