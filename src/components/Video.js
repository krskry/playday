import React from 'react';

const Video = ({ src }) => {
    return (
        <video autoplay loop >
            {/* <source src={src} type="video/webm" /> */}
            <source src={src} type="video/mp4" />
            {/* Your browser does not support the HTML5 video tag. */}
        </video>
    );
}
export default Video