import tw from "twin.macro";
export const PrimaryButton = tw.button`px-8 py-3  rounded bg-primary-500 text-gray-100 hocus:bg-primary-700 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;

export const SecondaryButton = tw.button`px-8 py-3  rounded bg-orange-500 text-gray-100 hocus:bg-orange-700 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;
