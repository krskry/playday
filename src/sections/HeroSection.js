import React from "react";
import styled, { css, keyframes } from "styled-components";
import tw from "twin.macro";
import { useTranslation } from "react-i18next";
import { motion } from "framer-motion";
import HeroImage from "../assets/HeroImage.jpg";
import Video from "../components/Video";
import StrumAway from "../assets/video1.mp4";
import Transition from "../components/transition";
import Video1 from "../assets/Strum-Away.mp4";
import SocialLinks from "../components/SocialLinks";
import tailwind from "../../tailwind.config.js";
import BgImage from "../components/BgImage";

const HeroNav = styled.div`
  bottom: 70px;
  right: 0;
  max-width: 140rem;
  padding: 2rem 5rem;
  width: 100%;
  ${tw`flex absolute justify-between text-white items-end z-10`}
  div {
    ${tw`flex`}
  }

  @media (max-width: ${tailwind.theme.screens.xl}) {
    display: none;
  }
`;

const NavItem = styled.div`
  cursor: pointer;
  display: inline-block;
  span:nth-of-type(2) {
    letter-spacing: 1.23px;
  }
  ${tw`flex flex-col text-white text-xl w-56 z-10`};
  span:first-of-type {
    position: relative;
    &:after {
      width: 0;
      opacity: 0;
      content: "";
      position: absolute;
      bottom: 5px;
      left: 0;
      height: 2px;
      background: white;
    }
  }
  span:nth-of-type(2) {
    padding-bottom: 1rem;
    border-bottom: 2px solid rgba(255, 255, 255, 0.2);
  }
  ${(props) =>
    props.isActive &&
    css`
      span:nth-of-type(2) {
        border-bottom: 2px solid rgaba(255, 255, 255, 0.1);
      }
    `}
`;

const NavDetails = styled.div`
  cursor: pointer;
  max-width: 400px;
  bottom: 240px;
  left: calc(50vw - 640px);
  ${tw`flex flex-col text-white text-5xl `};
  p {
    ${tw`flex flex-col text-white text-lg mb-0 z-10`};
  }
  span:nth-of-type(2) {
    letter-spacing: 1.23px;
  }
`;

const MarketingSlogan = styled.div`
  position: absolute;
  top: 30%;
  left: 0;
  width: 100%;
  z-index: 5;
  max-width: 140rem;
  padding: 2rem 5rem;
  display: flex;
  justify-content: space-between;
  h1 {
    font-size: 4.5rem;
    text-transform: uppercase;
    line-height: 125%;
    color: white;
    font-weight: 100;
    letter-spacing: 1.25px;
    strong {
      font-weight: 300;
    }
  }
  @media (max-width: ${tailwind.theme.screens.xl}) {
    display: none;
  }
`;

const MainVideoContainer = styled.div`
  height: 100vh;
  overflow: hidden;
  position: relative;
  background: rgba(41, 96, 147, 0.2);
  div {
    transition: opacity 0.78s cubic-bezier(0.17, 0.67, 0.72, 0.8);
  }
  &:after {
    content: "";
    opacity: 1 !important;
    position: absolute;
    width: 100vw;
    top: 0;
    left: 0;
    height: 100%;
    background: linear-gradient(
      to bottom,
      rgba(41, 96, 147, 0.8),
      rgba(41, 96, 147, 0.2)
    );
  }
`;

const propTypes = {};

const defaultProps = {};

function unfade(element) {
  let op = 0.67; // initial opacity
  element.style.display = "block";
  const timer = setInterval(function() {
    if (op >= 1) {
      clearInterval(timer);
    }
    element.style.opacity = op;
    element.style.filter = `alpha(opacity=${op * 10})`;
    op += op * 0.1;
  }, 20);
}

export default function HeroSection(props) {
  const { t } = useTranslation();
  const [currStep, setStep] = React.useState(1);
  const containerRef = React.useRef(null);

  const items = [
    {
      step: 1,
      title: t("home.hero_item_title1"),
      message: t("home.hero_item_message1"),
      details: t("home.hero_item_details1"),
    },
    {
      step: 2,
      title: t("home.hero_item_title2"),
      message: t("home.hero_item_message2"),
      details: t("home.hero_item_details2"),
    },
    {
      step: 3,
      title: t("home.hero_item_title3"),
      message: t("home.hero_item_message3"),
      details: t("home.hero_item_details3"),
    },
  ];

  const handleClick = (step) => {
    unfade(containerRef.current);
    setStep(step);
  };

  const returnMedia = (currStep) => {
    switch (currStep) {
      case 1:
        return <BgImage filename="hippo" />;
      case 2:
        return <BgImage filename="HeroImage" />;
      case 3:
        return <Video src={Video1} />;
      default:
        return "HeroImage";
    }
  };

  const currStepMsg = items.find((i) => i.step === currStep).message;
  const currDetails = items.find((i) => i.step === currStep).details;
  return (
    <MainVideoContainer>
      <div ref={containerRef}>{returnMedia(currStep)}</div>
      <MarketingSlogan>
        <h1>{currStepMsg}</h1>
        <SocialLinks />
        {/* <PlayBtn>dds</PlayBtn> */}
      </MarketingSlogan>
      <HeroNav>
        <NavDetails>
          <span>
            {currStep} / {items.length}
          </span>
          <p>{currDetails}</p>
        </NavDetails>
        <Transition start={true}>
          {items.map((i, index) => {
            const { step } = i;
            const stepString = `0${step}`;
            return (
              <NavItem
                isActive={step === currStep ? 1 : 0}
                onClick={() => handleClick(step)}
              >
                <span>{stepString}</span>
                <span>{i.title}</span>
              </NavItem>
            );
          })}
        </Transition>
      </HeroNav>
    </MainVideoContainer>
  );
}

HeroSection.propTypes = propTypes;
HeroSection.defaultProps = defaultProps;
