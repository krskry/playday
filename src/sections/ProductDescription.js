import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
import tailwind from "../../tailwind.config";
import parse from "html-react-parser";

const Container = tw.div`max-w-screen-xl mx-auto flex `;
const TwoColumn = tw.div`flex flex-col lg:flex-row bg-gray-100`;
const LeftColumn = tw.div`w-full ml-8 mr-8 xl:pl-10 py-8`;
const RightColumn = tw.div`ml-8 mr-8 xl:pl-10 py-8 max-w-2xl`;

const ProductDescription = ({ post, className }) => {
  return (
    <Container className={className}>
      <TwoColumn>
        <LeftColumn>
          <img src={post.imageSrc} />
        </LeftColumn>
        <RightColumn>{parse(post.html)}</RightColumn>
      </TwoColumn>
    </Container>
  );
};

export default styled(ProductDescription)`
  h1 {
    ${tw`pl-32 relative uppercase text-sm mb-4 mt-20`}
    &:after {
      position: absolute;
      top: calc(50% - 1px);
      left: 0px;
      width: 78px;
      height: 2px;
      content: "";
      background: ${tailwind.theme.colors.black};
    }
  }
  h2 {
    ${tw`capitalize text-5xl mb-8`}
  }
  article {
  }
`;
