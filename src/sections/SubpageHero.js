import React from 'react';
import styled from "styled-components"
import tw from "twin.macro";
import tailwind from "../../tailwind.config"

const Container = tw.div`max-w-screen-xl mx-auto pt-20 lg:pt-24 flex`;

const SingleColumn = tw.div`w-1/3 flex justify-center items-center`;
const Article = styled.article(props => [
    tw`mt-1 sm:mt-4 font-medium text-lg text-secondary-700 leading-loose`,
    props.isArticleFull ? tw`max-w-7xl` : tw`max-w-3xl mt-1 sm:mt-4 font-medium text-secondary-100 leading-loose`
]);
const DoubleColumn = styled.article(props => [
    props.isArticleFull ? tw`w-full` : tw`w-2/3`
])
const SubpageHero = ({ className, title = "title", subtitle = "subtitle", article = "", showVideo = false, isArticleFull = false, showNext }) => {
    return (
        <Container className={className}>
            <DoubleColumn isArticleFull={isArticleFull}>
                <h1>{title}</h1>
                <h2>{subtitle}</h2>
                <Article isArticleFull={isArticleFull}>{article}</Article>
            </DoubleColumn>
            {showNext && (<SingleColumn>
                {showNext()}
            </SingleColumn>)}
        </Container>
    );
}

export default styled(SubpageHero)`
h1{
   ${tw`pl-32 relative uppercase text-sm mb-4 mt-20`} 
    &:after{
        position:absolute;
        top:calc(50% - 1px);
        left:0px;
        width:78px;
        height:2px;
        content:'';
        background: ${tailwind.theme.colors.black}
    }
}
h2{
    ${tw`capitalize text-5xl mb-8`} 
}
article{
}
`;


