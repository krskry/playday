import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";
import ThreeColContactDetails from "../treact/components/cards/ThreeColContactDetails"
import SimpleWithSideImage from "../treact/components/faqs/SimpleWithSideImage"
import { useTranslation } from "react-i18next";
import SubpageHero from "../sections/SubpageHero"

import PortfolioTwoCardsWithImage from "../treact/components/cards/PortfolioTwoCardsWithImage";

const Next = () => {
  return (
    <video width="100%" height="100vh" preload='auto' autoPlay muted>
      <source src={require("../assets/video1.mp4")
      } type="video/mp4" />
      Your browser does not support HTML5 video.
    </video>
  )
}
const NotFoundPage = ({data}) => {
  const { t } = useTranslation();
  const items = data.allMarkdownRemark.edges.map(item => (item = item.node));

  return (
    <Layout>
      <SEO title={t("about.hero_title")} />
      <SubpageHero title={t("about.hero_title")} subtitle={t("about.hero_subtitle")} article={t("about.hero_article")} showNext={Next} />
      <ThreeColContactDetails />
      <SimpleWithSideImage />
      <PortfolioTwoCardsWithImage items={items} />
    </Layout>
  );
}

export default NotFoundPage;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/(portfolio)/" } }
    ) {
      edges {
        node {
          id
          html
          frontmatter {
            featuredPost
            date
            place
            description
            title
            featuredImage {
              childImageSharp {
                fluid(quality: 99) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  }
`;
