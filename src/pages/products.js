import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";
import GridWithfeaturedPost from "../treact/components/blogs/GridWithFeaturedPost";
import SubpageHero from "../sections/SubpageHero"
import { useTranslation } from "react-i18next";
import Button from "../components/btn"
const MapItems = ({ items }) => {
  return (
    <div>
      {items.map(item => <Button to={`/products${item.fields.slug}`} key={item.frontmatter.title}>  {item.frontmatter.title}</Button>)}
    </div>
  )
}

const NotFoundPage = ({ data, location }) => {
  const { t } = useTranslation();
  const items = data.allMarkdownRemark.edges.map(item => item = item.node)

  return (
    <Layout>
      <SEO title="404: Not found" />
      <SubpageHero title={t("products.hero_title")} subtitle={t("products.hero_subtitle")} article={t("products.hero_article")} showNext={() => MapItems({ items })} />
      <GridWithfeaturedPost items={items} />
    </Layout>
  );
}

export default NotFoundPage;


export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
      allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC } filter: {fileAbsolutePath: {regex: "/(products)/" }})  {
        edges {
          node {
            id
            html
            frontmatter {
              featuredPost
              date
              description
              title
              tags
              featuredImage {
                childImageSharp {
                    fluid(quality: 99) {
                      ...GatsbyImageSharpFluid_withWebp_noBase64
                    }
                  }
                }
            }
            fields {
              slug
            }
          }
        }
      }
    }  
    `