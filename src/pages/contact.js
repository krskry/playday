import React from "react";
import { FaMapMarkerAlt, FaPhone, FaEnvelope, FaClock } from "react-icons/fa";
import tw from "twin.macro";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Form from "../components/Form";
import TwoColContactUsWithIllustrationFullForm from "../treact/components/forms/TwoColContactUsWithIllustrationFullForm";
import SubpageHero from "../sections/SubpageHero";
import SocialLinks from "../components/SocialLinks";

const TwoColumn = tw.div`pt-24 pb-32 px-4 flex justify-between items-center flex-col lg:flex-row`;
const LeftColumn = tw.div`flex flex-col items-center lg:block`;
const RightColumn = tw.div`w-full sm:w-5/6 lg:w-1/2 mt-16 lg:mt-0 lg:pl-8`;
const Information = tw.div`w-full sm:w-5/6 lg:w-1/2 mt-16 lg:mt-0 lg:pl-8`;

const Container = styled.div`
  ${tw`flex-col items-center justify-center w-1/2 text-white z-10 text-lg mt-8`}
  svg {
    ${tw`text-white mr-12`}
  }
  p,
  div {
    ${tw`flex mb-8`}
  }
  span {
    ${tw`font-bold`}
  }
`;

const LeftCol = () => {
  const { t } = useTranslation();
  return (
    <Container>
      <p>
        <FaMapMarkerAlt />
        <span> {t("contact.adress_icon_title")}</span>
      </p>
      <p>{t("contact.adress_icon_description")}</p>
      <p>
        <Information> {t("contact.information")}</Information>
      </p>
      <p>
        <FaPhone />
        <span> {t("contact.phone_icon_title")}</span>
      </p>
      <p>
        <FaClock />
        <span> {t("contact.clock_icon_title")}</span>
      </p>
      <p>{t("contact.clock_icon_description")}</p>
      <div>{t("contact.phone_icon_description")}</div>
      <p>
        <FaEnvelope />
        <span> {t("contact.envelope_icon_title")}</span>
      </p>
      <p> {t("contact.envelope_icon_description")}</p>
      <p>
        <FaMapMarkerAlt />
        <span> {t("contact.adress_icon_title2")}</span>
      </p>
      <div>{t("contact.adress_icon_description2")}</div>
    </Container>
  );
};

const NotFoundPage = () => {
  const { t } = useTranslation();

  return (
    <Layout>
      <SEO title={t("contact.hero_title")} />
      <SubpageHero
        title={t("contact.hero_title")}
        subtitle={t("contact.hero_subtitle")}
        isArticleFull
      />
      <TwoColContactUsWithIllustrationFullForm
        llForm
        LeftCol={LeftCol}
        submitButtonText={t("contact.submitButtonText")}
        formAction={t("contact.formAction")}
        formMethod={t("contact.formMethod")}
      />
    </Layout>
  );
};

export default NotFoundPage;
