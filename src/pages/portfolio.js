import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";
import PopularAndRecentBlogPosts, {
  SingleBlogPost
} from "../treact/components/blogs/PopularAndRecentBlogPosts";
import { useTranslation } from "react-i18next";
import SubpageHero from "../sections/SubpageHero";

const NotFoundPage = ({ data, location }) => {
  const { t } = useTranslation();
  const items = data.allMarkdownRemark.edges.map(item => (item = item.node));
  return (
    <Layout>
      <SEO title={t("portfolio.hero_title")} />
      <SubpageHero
        title={t("portfolio.hero_title")}
        subtitle={t("portfolio.hero_subtitle")}
        article={t("portfolio.hero_article")}
        showNext={() => SingleBlogPost({ items })}
      />
      <PopularAndRecentBlogPosts items={items} />
    </Layout>
  );
};

export default NotFoundPage;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/(portfolio)/" } }
    ) {
      edges {
        node {
          id
          html
          frontmatter {
            featuredPost
            place
            date
            description
            title
            featuredImage {
              childImageSharp {
                fluid(quality: 99) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  }
`;
