import React from "react";
import { useTranslation } from "react-i18next";
import { graphql } from "gatsby";

import GetStarted from "../treact/components/cta/GetStarted";
import Layout from "../components/layout";
import Partners from "../components/partners";
import SEO from "../components/seo";
import Features from "../components/Features";
import Testimonials from "../components/testimonials";
import FeaturedPortfolio from "../components/FeaturedPortfolio";
import HeroSection from "../sections/HeroSection";

const IndexPage = ({ data, location }) => {
  const { t } = useTranslation();
  const items = data.allMarkdownRemark.edges.map((item) => (item = item.node));

  return (
    <Layout>
      <SEO title="Home" />
      <HeroSection />
      <Features
        cards={[
          {
            imageSrc: "feature1",
            subtitle: t("home.features_subheader1"),
            title: t("home.features_header1"),
            description: t("home.features_description1"),
            url: "https://timerse.com",
          },

          {
            imageSrc: "Card3",
            subtitle: t("home.features_subheader2"),
            title: t("home.features_header2"),
            description: t("home.features_description2"),
            url: "https://timerse.com",
          },

          {
            imageSrc: "Construction3",
            subtitle: t("home.features_subheader3"),
            title: t("home.features_header3"),
            description: t("home.features_description3"),
            url: "https://timerse.com",
          },
          {
            imageSrc: "Construction2",
            subtitle: t("home.features_subheader4"),
            title: t("home.features_header4"),
            description: t("home.features_description4"),
            url: "https://timerse.com",
          },
        ]}
      />
      <Testimonials />
      <Partners />
      <FeaturedPortfolio
        items={items}
        subheading={t("home.featuredPortfolio_subheading")}
        headingHtmlComponent={t("home.featuredPortfolio_headingHtmlComponent")}
        description={t("home.featuredPortfolio_description")}
        linkText={t("home.featuredPortfolio_linkText")}
        cardLinkText={t("home.featuredPortfolio_cardLinkText")}
        textOnLeft={t("home.featuredPortfolio_textOnLeft")}
      />
      <GetStarted
        text={t("home.getStarted_text")}
        primaryLinkText={t("home.getStarted_primaryLinkText")}
        primaryLinkUrl={t("home.getStarted_primaryLinkUrl")}
        secondaryLinkText={t("home.getStarted_secondaryLinkText")}
        secondaryLinkUrl={t("home.getStarted_secondaryLinkUrl")}
      />
    </Layout>
  );
};

export default IndexPage;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/(portfolio)/" } }
    ) {
      edges {
        node {
          id
          html
          frontmatter {
            featuredPost
            date
            place
            description
            title
            featuredImage {
              childImageSharp {
                fluid(quality: 90, maxWidth: 384) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  }
`;
