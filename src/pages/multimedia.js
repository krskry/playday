import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

import VerticalWithAlternateImageAndText from "../treact/components/features/VerticalWithAlternateImageAndText";

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <VerticalWithAlternateImageAndText />
  </Layout>
);

export default NotFoundPage;
