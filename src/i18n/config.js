/* eslint-disable global-require */
import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

const detectionOptions = {
  order: [
    "path",
    "cookie",
    "navigator",
    "localStorage",
    "subdomain",
    "queryString",
    "htmlTag",
  ],
  lookupFromPathIndex: 0,
};

i18next
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    detection: detectionOptions,
    fallbackLng: "pl",
    react: {
      useSuspense: false,
    },
    resources: {
      pl: {
        home: require("./translations/pl/home.json"),
        shared: require("./translations/pl/shared.json"),
        about: require("./translations/pl/about.json"),
        contact: require("./translations/pl/contact.json"),
        products: require("./translations/pl/products.json"),
        portfolio: require("./translations/pl/portfolio.json"),
      },
    },
    ns: [
      "home",
      "shared",
      "about",
      "products",
      "contact",
      "newsroom",
      "portfolio",
    ],
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    keySeparator: false,
    nsSeparator: ".",
    returnObjects: true,
    debug: process.env.NODE_ENV === "development",
  });

i18next.languages = ["pl"];

export default i18next;
