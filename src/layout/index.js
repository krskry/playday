
import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "../assets/stylesheets/theme";
// import Header from "../components/shared/header";
// import Footer from "/.components/footer";
import { withTrans } from "../i18n/withTrans";


const Layout = ({ children }) => (
  <ThemeProvider theme={theme}>
    <Header siteTitle={data.site.siteMetadata.title} />
    {/* <GlobalStyle /> */}
    <main>{children}</main>
    <Footer />
  </ThemeProvider>

);


export default withTrans(Layout);
