import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
import { useTranslation } from "react-i18next";
import DotPattern from "../../images/dot-pattern.svg";
import { SectionHeading as HeadingTitle } from "../misc/Headings.js";
import Image1 from "../../../assets/feature1.jpg";
import Image2 from "../../../assets/Card3.jpg";
import Image3 from "../../../assets/Construction3.jpg";
import Image4 from "../../../assets/Construction2.jpg";

const Container = tw.div`relative`;

const SingleColumn = tw.div`max-w-full mx-auto py-20 lg:py-24`;

const HeadingInfoContainer = tw.div`flex flex-col items-center`;
const HeadingDescription = tw.p`mt-4 font-medium text-gray-600 text-center max-w-sm`;

const Content = tw.div`mt-16`;

const Card = styled.div((props) => [
  tw`mt-24 md:flex justify-center items-center`,
  props.reversed ? tw`flex-row-reverse` : "flex-row",
]);
const Image = styled.div((props) => [
  `background-image: url("${props.imageSrc}");`,
  tw`rounded w-1/2  flex-shrink-0 md:h-72 bg-cover bg-center mx-4 sm:mx-8 md:mx-4 lg:mx-8`,
]);
const Details = tw.div`mt-4 md:mt-0 md:max-w-md mx-4 sm:mx-8 md:mx-4 lg:mx-8`;
const Subtitle = tw.div`font-bold tracking-wide text-secondary-100`;
const Title = tw.h4`text-3xl font-bold text-gray-900`;
const Description = tw.p`mt-2 text-sm leading-loose`;
const Link = tw.a`inline-block mt-4 text-sm text-primary-500 font-bold cursor-pointer transition duration-300 border-b-2 border-transparent hover:border-primary-500`;

const SvgDotPattern1 = tw(
  DotPattern
)`absolute top-0 left-0 transform -translate-x-20 rotate-90 translate-y-8 -z-10 opacity-25 text-primary-500 fill-current w-24`;
const SvgDotPattern2 = tw(
  DotPattern
)`absolute top-0 right-0 transform translate-x-20 rotate-45 translate-y-24 -z-10 opacity-25 text-primary-500 fill-current w-24`;
const SvgDotPattern3 = tw(
  DotPattern
)`absolute bottom-0 left-0 transform -translate-x-20 rotate-45 -translate-y-8 -z-10 opacity-25 text-primary-500 fill-current w-24`;
const SvgDotPattern4 = tw(
  DotPattern
)`absolute bottom-0 right-0 transform translate-x-20 rotate-90 -translate-y-24 -z-10 opacity-25 text-primary-500 fill-current w-24`;

export default () => {
  const { t } = useTranslation();
  const cards = [
    {
      imageSrc: "feature1",
      subtitle: t("home.features_subheader1"),
      title: t("home.features_header1"),
      description: t("home.features_description1"),
      url: "https://timerse.com",
    },

    {
      imageSrc: "Card3",
      subtitle: t("home.features_subheader2"),
      title: t("home.features_header2"),
      description: t("home.features_description2"),
      url: "https://timerse.com",
    },

    {
      imageSrc: "Construction3",
      subtitle: t("home.features_subheader3"),
      title: t("home.features_header3"),
      description: t("home.features_description3"),
      url: "https://timerse.com",
    },
    {
      imageSrc: "Construction2",
      subtitle: t("home.features_subheader4"),
      title: t("home.features_header4"),
      description: t("home.features_description4"),
      url: "https://timerse.com",
    },
  ];

  return (
    <Container>
      <SingleColumn>
        <HeadingInfoContainer>
          <HeadingTitle>{t("home.features_title_header")}</HeadingTitle>
          <HeadingDescription>
            {t("home.features_title_subheader")}
          </HeadingDescription>
        </HeadingInfoContainer>
        <Content>
          {cards.map((card, i) => (
            <Card key={i} reversed={i % 2 === 1}>
              <Image imageSrc={card.imageSrc} style={{ height: "10vw" }} />
              <Details>
                <Subtitle>{card.subtitle}</Subtitle>
                <Title>{card.title}</Title>
                <Description>{card.description}</Description>
                <Link href={card.url}>See Event Details</Link>
              </Details>
            </Card>
          ))}
        </Content>
      </SingleColumn>
      <SvgDotPattern1 />
      <SvgDotPattern2 />
      <SvgDotPattern3 />
      <SvgDotPattern4 />
    </Container>
  );
};
