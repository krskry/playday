import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { SectionHeading as HeadingTitle } from "../misc/Headings.js";
import SvgDecoratorBlob1 from "../../images/svg-decorator-blob-1.svg";
import SvgDecoratorBlob2 from "../../images/svg-decorator-blob-3.svg";
import { useTranslation } from "react-i18next";

import Card1Image from "../../../assets/Card1.jpg";
import Card2Image from "../../../assets/HeroImage.jpg";
import Card3Image from "../../../assets/Card3.jpg";

const Container = tw.div`relative`;
const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;
const ThreeColumn = tw.div`flex flex-col items-center lg:items-stretch lg:flex-row flex-wrap`;
const Column = tw.div`mt-24 lg:w-1/3`;

const HeadingInfoContainer = tw.div`flex flex-col items-center`;
// const HeadingDescription = tw.p`mt-4 font-medium text-gray-600 text-center max-w-sm`;

const Card = styled.div`
  ${tw`lg:mx-4 xl:mx-8 max-w-sm lg:max-w-2xl shadow-alt rounded-lg overflow-hidden`}
  transition:cubic-bezier(0.4, 0, 1, 1) .23s;
  cursor: pointer;
  &:hover {
    transform: translateY(-2.5px);
  }
`;

const Image = styled.div(props => [
  `background-image: url("${props.imageSrc}");`,
  tw`bg-cover bg-center h-80 lg:h-60`
]);

const Message = tw.div`p-2 mt-4 text-secondary-100 font-bold text-sm`;
const Title = tw.h4`p-2 mt-4 leading-relaxed font-bold text-lg`;
// const Link = tw.a`p-2 w-full pb-8 inline-block mt-2 text-sm text-primary-500 font-bold cursor-pointer transition duration-300 border-b-2 border-transparent hover:border-primary-500`;

const DecoratorBlob1 = tw(
  SvgDecoratorBlob1
)`-z-10 absolute bottom-0 right-0 w-48 h-48 transform translate-x-40 -translate-y-8 opacity-25`;
const DecoratorBlob2 = tw(
  SvgDecoratorBlob2
)`-z-10 absolute top-0 left-0 w-48 h-48 transform -translate-x-32 translate-y-full opacity-25`;

export default () => {
  const { t } = useTranslation();

  const blogPosts = [
    {
      imageSrc: Card1Image,
      title: t("service_title_1"),
      message: t("service_title_1_message"),
      url: "https://timerse.com"
    },
    {
      imageSrc: Card2Image,
      title: t("service_title_2"),
      message: t("service_title_2_message"),
      url: "https://reddit.com"
    },
    {
      imageSrc: Card3Image,
      title: t("service_title_3"),
      message: t("service_title_3_message"),
      url: "https://timerse.com"
    }
  ];
  return (
    <Container>
      <Content>
        <HeadingInfoContainer>
          <HeadingTitle
            data-sal="fade"
            data-sal-delay="100"
            data-sal-easing="easeInOutQuint"
            data-sal-duration="1200"
          >
            {t("service_header")}
          </HeadingTitle>
          {/* <HeadingDescription>Some amazing blog posts that are written by even more amazing people.</HeadingDescription> */}
        </HeadingInfoContainer>
        <ThreeColumn>
          {blogPosts.map((post, index) => (
            <Column key={index}>
              <Card
                data-sal="fade"
                data-sal-delay={index * 100 + 100}
                data-sal-easing="easeInOutQuint"
                data-sal-duration="1200"
                className="pb-4"
              >
                <Image imageSrc={post.imageSrc} />
                <Title>{post.title}</Title>
                <Message>{post.message}</Message>
                <Message> {t("service_read_more")}</Message>
              </Card>
            </Column>
          ))}
        </ThreeColumn>
      </Content>
      <DecoratorBlob1 />
      <DecoratorBlob2 />
    </Container>
  );
};
