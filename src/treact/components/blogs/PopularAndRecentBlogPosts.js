import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { motion } from "framer-motion";
import { css } from "styled-components/macro"; //eslint-disable-line
import { SectionHeading } from "../misc/Headings.js";
import { Container, ContentWithPaddingXl } from "../misc/Layouts.js";

const Row = tw.div`flex flex-row lg:flex-row -mb-10`;
const Heading = tw(SectionHeading)`text-left lg:text-base xl:text-lg`;

const PopularPostsContainer = tw.div`flex`;
const PostsContainer = tw.div`mt-12 flex flex-row justify-between flex-wrap`;
const PostsContainerCol = tw.div`mt-12 flex flex-col justify-between`;

const Post = tw(
  motion.a
)`w-full lg:w-1/4  block sm:max-w-sm cursor-pointer mb-16 last:mb-0 sm:mb-0 sm:odd:mr-8 lg:mr-8 xl:mr-16`;
const PostFeatured = tw(
  motion.a
)`w-full block  w-96 cursor-pointer mb-16 last:mb-0 sm:mb-0 mt-64`;
const Image = styled(motion.div)(props => [
  `background-image: url("${props.imageSrc}");`,
  tw`h-64 bg-cover bg-center rounded`
]);
const Title = tw.h5`mt-6 text-xl font-bold transition duration-300 group-hover:text-primary-500`;
const Description = tw.p`mt-2 font-medium text-secondary-100 leading-loose text-sm`;
const AuthorInfo = tw.div`mt-6 flex items-center`;
const AuthorImage = tw.img`w-12 h-12 rounded-full`;
const AuthorNameAndProfession = tw.div`ml-4`;
const AuthorName = tw.h6`font-semibold text-lg`;
const AuthorProfile = tw.p`text-secondary-100 text-sm`;

const RecentPostsContainer = styled.div`
  ${tw`mt-24 lg:mt-0 lg:w-1/3`}
  ${PostsContainer} {
    ${tw`flex flex-wrap flex-col`}
  }
  ${Post} {
    ${tw`flex justify-between mb-10 max-w-none w-full sm:w-1/2 lg:w-auto sm:odd:pr-12 lg:odd:pr-0 mr-0`}
  }
  ${Title} {
    ${tw`text-base xl:text-lg mt-0 mr-4 lg:max-w-xs`}
  }
  ${AuthorName} {
    ${tw`mt-3 text-sm text-secondary-100 font-normal leading-none`}
  }
  ${Image} {
    ${tw`h-20 w-20 flex-shrink-0`}
  }
`;
const PostTextContainer = tw.div``;

export const SingleBlogPost = ({ items }) => {
  const postBackgroundSizeAnimation = {
    rest: {
      backgroundSize: "100%"
    },
    hover: {
      backgroundSize: "110%"
    }
  };

  items.map(item => {
    item.url = `/portfolio${item.fields.slug}`;
    item.authorName = item.frontmatter.date;
    item.description = item.frontmatter.description;
    item.title = item.frontmatter.title;
    item.postImageSrc =
      item.frontmatter.featuredImage.childImageSharp.fluid.src;
  });

  const post = items.find(item => item.frontmatter.featuredPost);

  //
  return (
    <Container>
      <PostFeatured
        href={post.url}
        className="group"
        initial="rest"
        whileHover="hover"
        animate="rest"
      >
        <Image
          transition={{ duration: 0.3 }}
          variants={postBackgroundSizeAnimation}
          imageSrc={post.postImageSrc}
        />
        <Title>{post.title}</Title>
        <Description>{post.description}</Description>
        {/* <AuthorInfo>
                    <AuthorImage src={post.authorImageSrc} />
                    <AuthorNameAndProfession>
                      <AuthorName>{post.authorName}</AuthorName>
                      <AuthorProfile>{post.authorProfile}</AuthorProfile>
                    </AuthorNameAndProfession>
                  </AuthorInfo> */}
      </PostFeatured>
    </Container>
  );
};

export default ({ items }) => {
  // This setting is for animating the post background image on hover
  const postBackgroundSizeAnimation = {
    rest: {
      backgroundSize: "100%"
    },
    hover: {
      backgroundSize: "110%"
    }
  };

  // Recommended: Only 2 Items
  items.map(item => {
    item.url = `/portfolio${item.fields.slug}`;
    item.authorName = item.frontmatter.date;
    item.description = item.frontmatter.description;
    item.title = item.frontmatter.title;
    item.postImageSrc =
      item.frontmatter.featuredImage.childImageSharp.fluid.src;
  });

  // featuredPost


  const popularPosts = items;

  // [
  //   {
  //     postImageSrc:
  //       "https://images.unsplash.com/photo-1500530855697-b586d89ba3ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=640&q=80",
  //     authorImageSrc:
  //       "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3.25&w=512&h=512&q=80",
  //     title: "Tips on how to travel safely in foreign countries",
  //     description:
  //       "Lorem ipsum dolor sit amet, consecteturious adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua now ele.",
  //     authorName: "Charlotte Delos",
  //     authorProfile: "Travel Advocate",
  //     url: "https://timerse.com"
  //   },
  //   {
  //     postImageSrc:
  //       "https://images.unsplash.com/photo-1563784462041-5f97ac9523dd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=640&q=80",
  //     authorImageSrc:
  //       "https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=512&h=512&q=80",
  //     title: "Enjoying the beach life while on a vacation",
  //     description:
  //       "Lorem ipsum dolor sit amet, consecteturious adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua now ele.",
  //     authorName: "Adam Cuppy",
  //     authorProfile: "Vlogger",
  //     url: "https://reddit.com"
  //   }
  // ];

  const recentPosts = items.filter(x => !x.frontmatter.featuredPost);

  return (
    <Container>
      <ContentWithPaddingXl>
        <Heading>Popular Posts</Heading>

        <Row>
          <PopularPostsContainer>
            <PostsContainer>
              {popularPosts.map((post, index) => (
                <Post
                  key={index}
                  href={post.url}
                  className="group"
                  initial="rest"
                  whileHover="hover"
                  animate="rest"
                >
                  <Image
                    transition={{ duration: 0.3 }}
                    variants={postBackgroundSizeAnimation}
                    imageSrc={post.postImageSrc}
                  />
                  <Title>{post.title}</Title>
                  <Description>{post.description}</Description>
                  {/* <AuthorInfo>
                    <AuthorImage src={post.authorImageSrc} />
                    <AuthorNameAndProfession>
                      <AuthorName>{post.authorName}</AuthorName>
                      <AuthorProfile>{post.authorProfile}</AuthorProfile>
                    </AuthorNameAndProfession>
                  </AuthorInfo> */}
                </Post>
              ))}
            </PostsContainer>
          </PopularPostsContainer>
        </Row>
      </ContentWithPaddingXl>
    </Container>
  );
};
