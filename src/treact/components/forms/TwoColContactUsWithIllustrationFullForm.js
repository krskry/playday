import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import {
  SectionHeading,
  Subheading as SubheadingBase
} from "../misc/Headings.js";
import { PrimaryButton as PrimaryButtonBase } from "../misc/Buttons.js";
import EmailIllustrationSrc from "../../../assets/map.jpg";
import Form from "../../../components/Form";

const Container = tw.div`relative mt-12`;
const TwoColumn = tw.div`flex flex-col md:flex-row justify-between max-w-screen-xl mx-auto pb-20 md:pb-24`;
const Column = tw.div`w-full max-w-md mx-auto md:max-w-none md:mx-0`;
const ImageColumn = tw(Column)`md:w-5/12 flex-shrink-0 h-80 md:h-auto`;

const TextColumn = styled(Column)(props => [
  tw`md:w-7/12 md:mt-0`,
  props.textOnLeft
    ? tw`md:mr-12 lg:mr-16 md:order-first`
    : tw`md:ml-12 lg:ml-16 md:order-last`
]);

const Image = styled.div(props => [
  `background-image: url("${props.imageSrc}");
  opacity:.9;
  &:after {
    content: "";
    opacity: 1 !important;
    position: absolute;
    width: 100%;
    top: 0;
    left: 0;
    height: 100%;
    background: linear-gradient(
      to top,
      rgba(41, 96, 147, 0.1),
      rgba(41, 96, 147, 0.3)
    );
  }`,
  tw`rounded bg-cover bg-no-repeat bg-center p-8 w-full flex justify-center items-center relative`
]);
const TextContent = tw.div`lg:py-8 text-center md:text-left flex items-center justify-center`;

const Subheading = tw(SubheadingBase)`text-center md:text-left`;
const Heading = tw(
  SectionHeading
)`mt-4 font-black text-left text-3xl sm:text-4xl lg:text-5xl text-center md:text-left leading-tight`;
const Description = tw.p`mt-4 text-center md:text-left text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100`;

const StyledForm = styled.div`
  border: 1px solid red;
  form {
    ${tw`mt-8 md:mt-10 text-sm flex flex-col max-w-sm mx-auto md:mx-0`}
    input {
      ${tw`mt-6 first:mt-0 border-b-2 py-3 focus:outline-none font-medium transition duration-300 hocus:border-primary-500`}
    }
  }
`;

export default ({
  subheading = "",
  heading = "",
  description = "",
  submitButtonText = "Send",
  formAction = "#",
  formMethod = "get",
  textOnLeft = false,
  LeftCol = <div />
}) => {
  return (
    <Container>
      <TwoColumn>
        <TextColumn textOnLeft={textOnLeft}>
          <TextContent>
            {subheading && <Subheading>{subheading}</Subheading>}
            <Heading>{heading}</Heading>
            {description && <Description>{description}</Description>}
            <Form />
          </TextContent>
        </TextColumn>
        <ImageColumn>
          <Image imageSrc={EmailIllustrationSrc}>
            <LeftCol />
          </Image>
        </ImageColumn>
      </TwoColumn>
    </Container>
  );
};
