---
templateKey: portfolio-post
title: Leopark-park
place: Wrocław
item_name: Leopark
date: 2020-12-25T19:04:19.444Z
description: |-
  Leopark-park
  Portfolio item name: Leopark
  Fri Dec 25 2020 20:04:19 GMT+0100 (czas środkowoeuropejski standardowy)
featuredPost: true
featuredImage: /img/11046347_863729920372184_4656957680051387255_o.jpg
tags:
  - tag1 tag2
---

Podsumowanie 2020 r. w fizyce. Trzy wybrane i omówione zagadnienia, uznane przez redakcję Quanta Magazine, za najbardziej przełomowe. 1. Badania nad paradoksem informacyjnym czarnych dziur. 2. Badania nad nadprzewodnictwem w temp. pokojowej. 3. Uchwycenie źródła wysyłającego Fast Radio Bursts (FRB).
