---
templateKey: product-post
title: electric-cars
ProductName: Elektryczne samochody i motocykle
date: 2020-12-26T17:43:35.543Z
description: |+
  ścigaj się jak zawodowiec

featuredPost: false
featuredImage: /img/13217043_1014394085305766_5917864906303076364_o.jpg
---
Dla wszystkich fanów motoryzacji, którzy chcieli by choć na chwilę stać się kierowcą Formuły 1, mamy specjalnie zaprojektowany tor gokartowy, z mnóstwem zakrętów, znaków drogowych, a nawet stacją paliw z dystrybutorami.

Elektryczne samochodziki umożliwiają szalone wyścigi i wzajemną rywalizację, stanowiąc doskonały element każdego placu zabaw niezbędnego np. podczas imprez urodzinowych.

W ofercie posiadamy wiele modeli pojazdów elektrycznych, od wyścigówek, po motocykle a nawet quady. Z pewnością każdy znajdzie coś dla siebie.

Aby zabawa była w pełni bezpieczna pojazdy poruszają się po specjalnie do tego celu wybudowanym torze. Boki toru wyposażone są w specjalne barierki ochronne z wbudowanymi gumowymi amortyzatorami. System ten zapobiega twardym uderzeniom pojazdów, co znacząco podnosi bezpieczeństwo oraz zapobiega niszczeniu pojazdów. Dodatkowo moduły mogą zostać wyposażone w płotek, tak by tor tworzył oddzielną strefę. Wszystkie pojazdy przeznaczone są do pracy na wielu powierzchniach takich jak beton, asfalt lub innych gładkich powierzchniach. Ponieważ tor jest projektowany od podstaw, możemy idealnie dopasować go do istniejącej przestrzeni. Może stanowić integralny element umieszczony pod konstrukcją zabawową lub osobną atrakcję.

Z samochodzików mogą samodzielnie korzystać dzieci powyżej 4 lat. Mniejsze zapraszamy do przejażdżki z rodzicami lub starszym rodzeństwem.

Urządzenia mogą stanowić dodatkowe źródło dochodu dla sali zabaw, ponieważ posiadają system uruchamiania przy pomocy monety lub żetonów.


Specyfikacja:
specjalistyczny tor, z profesjonalną barierką zabezpieczającą,

pojazd napędzany przy pomocy akumulatorów,

pojemność max. 80Ah zapewnia około 4h pracy bez ładowania,

prędkość regulowana w zakresie od 1km do 10 km/h.

czas jazdy regulowany w zakresie od 30 sek. do 3 min.,

pojazd zabezpieczony dookoła powierzchnią amortyzującą.

budowa i montaż zgodnie z normą EN 1176/77.

