---
templateKey: product-post
title: Volcanos
ProductName: Wulkan do wspinaczki
date: 2020-12-26T17:05:32.874Z
description: wspaniała zabawa dla każdego
featuredPost: false
featuredImage: /img/12605328_947833368628505_1938092131328232349_o.jpg
galleryImages: []
---


Co zrobić aby dostarczyć dzieciom jeszcze więcej niezapomnianych wrażeń, i aby sale zabaw stały się jeszcze bardziej atrakcyjne?

Wieloletnie doświadczenie, ciągłe udoskonalanie konstrukcji oraz użytych materiałów pomogło stworzyć unikalny produkt jakim jest Wulkan do wspinaczki.

Wulkany wspinaczkowe stanowią niezwykłe połączenie zalet ścianki wspinaczkowej i zjeżdżalni, tworząc trudną do pokonania i stanowiącą wyzwanie przeszkodę. Jednocześnie w każdym momencie wspinaczki dają możliwość bezpiecznego zjazdu w dół po miękkiej ścianie, będąc niezwykle bezpieczne nawet dla najmłodszych dzieci.

Poprzez swoje gabaryty, kształt oraz niezwykle atrakcyjną kolorystykę, wulkan wspinaczkowy jest jednym z bardziej pożądanych przez dzieci elementem placu zabaw. Ogromna kolorowa konstrukcja zaprasza by wspiąć się na sam szczyt, aby następnie … po prostu zjechać w dół śmiejąc się i krzycząc.

Wulkan do wspinaczki posiada 4 różne trasy, od 15 do 20 uchwytów, tak by każde dziecko mogło wspinać się z odpowiednim dla niego stopniem trudności. Na górze wulkanu (4m lub 5m) znajduje się taras widokowy o średnicy 2,5 m, na którym dzieci mogą odpocząć po wyczerpującej wspinaczce. Aby zabawa stała się jeszcze bardziej ekscytująca, do tarasu widokowego można zamontować zjeżdżalnię. Do wyboru jest kilka typów zjeżdżalni: stalowa rynnowa, rurowa, miękka. Na dole wokół całej konstrukcji, w miejscu styku wulkanu z podłogą, zamontowane są specjalne miękkie maty zabezpieczające przed twardym upadkiem.

Konstrukcja wykonana jest z materiałów najwyższej jakości, zapewniających bardzo dużą wytrzymałość i trwałość elementów. Pokrycie wykonane jest z bardzo grubego, najwyższej jakości winylu. Całość wykonana jest z materiałów umożliwiających montaż zarówno wewnątrz i na zewnątrz.

Posiadamy w ofercie kilka opcji kolorystycznych oraz motywów. Istnieje również możliwość dostosowania nadruku do Państwa wymagań. Całość zaprojektowana i wykonana jest w taki sposób, by spełniała najbardziej restrykcyjne europejskie normy bezpieczeństwa.

Wulkan jest dostępny w dwóch wersjach 360° (pełna) i 180° (półokrąg).

##### Specyfikacja:

* niezwykle wytrzymały materiał PVC,
* zgrzewane szwy o wysokiej gęstości,
* specjalna mata zabezpieczająca wokół konstrukcji,
* możliwość wyboru dowolnego koloru, oraz łączenia kilku kolorów,
* możliwość nadruku motywu na powierzchni PVC,
* do wyboru kilka modeli zjeżdżalni mocowanych do konstrukcji,
* możliwość montażu zarówno wewnątrz jak i na zewnątrz,
* budowa i montaż zgodnie z normą EN 1176/77.